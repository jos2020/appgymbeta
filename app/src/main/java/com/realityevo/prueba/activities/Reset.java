package com.realityevo.prueba.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.SignInMethodQueryResult;
import com.realityevo.prueba.R;

public class Reset extends AppCompatActivity {

    private FirebaseAuth rAuth;
    private Button reset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset);

        rAuth = FirebaseAuth.getInstance();
        reset = findViewById(R.id.send);
        final TextView emailReset = findViewById(R.id.resetEmail);


        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rAuth.fetchSignInMethodsForEmail(emailReset.getText().toString()).addOnCompleteListener(new OnCompleteListener<SignInMethodQueryResult>() {
                    @Override
                    public void onComplete(@NonNull Task<SignInMethodQueryResult> task) {

                        if(task.getResult().getSignInMethods().isEmpty()){
                            Toast.makeText(Reset.this,"This email its not register in data base", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            rAuth.sendPasswordResetEmail(emailReset.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if(task.isSuccessful()){
                                          Toast.makeText(Reset.this,"send message , please check your inbox ",Toast.LENGTH_LONG).show();

                                    }
                                    else {
                                        Toast.makeText(Reset.this,task.getException().getMessage(),Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });


                        }

                    }
                });
            }
        });

    }
}