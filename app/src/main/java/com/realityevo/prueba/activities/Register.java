package com.realityevo.prueba.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.realityevo.prueba.R;

import java.util.HashMap;
import java.util.Objects;

public class Register extends AppCompatActivity {

    private EditText userName,lastName,emailAddress,password,tel;
    private Button registerBtn;
    private FirebaseAuth firebaseAuth;
    private ProgressBar progressBar;
    private DatabaseReference databaseReference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
//        Toolbar myToolbar = findViewById(R.id.toolbar2);
//        setSupportActionBar(myToolbar);
//        getSupportActionBar().setTitle("Register");
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(Register.this,Login.class));
//            }
//        });
        firebaseAuth = FirebaseAuth.getInstance();
        userName =findViewById(R.id.name);
        lastName =findViewById(R.id.lastName);
        emailAddress =findViewById(R.id.emailRegister);
        password =findViewById(R.id.passswordRegister);
        tel =findViewById(R.id.celNumber);
        registerBtn =findViewById(R.id.registerButton);
        progressBar = findViewById(R.id.progressBar);

        registerBtn.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final String user_name = userName.getText().toString();
            final String last_name = lastName.getText().toString();
            final String email = emailAddress.getText().toString();
            final String txt_password = password.getText().toString();
            final String txt_tel = tel.getText().toString();

                if(TextUtils.isEmpty(user_name) || TextUtils.isEmpty(last_name) || TextUtils.isEmpty(email) || TextUtils.isEmpty(txt_password) ||
                        TextUtils.isEmpty(txt_tel)){
                    Toast.makeText(Register.this, "All fields are required", Toast.LENGTH_SHORT).show();

                }

                else{
                    register(user_name,last_name,email,txt_password,txt_tel);
                }

            }



    });

    }

    private void register(final String user_name, final String last_name, final String email, final String txt_password, final String txt_tel) {
        progressBar.setVisibility(View.VISIBLE);
        firebaseAuth.createUserWithEmailAndPassword(email,txt_password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    FirebaseUser rUser = firebaseAuth.getCurrentUser();
                    String userId = rUser.getUid();
                    databaseReference = FirebaseDatabase.getInstance().getReference("Users").child(userId);
                    HashMap<String,String> hashMap = new HashMap<>();
                    hashMap.put("userId",userId);
                    hashMap.put("userName",user_name);
                    hashMap.put("lastName",last_name);
                    hashMap.put("emailAddress",email);
                    hashMap.put("password",txt_password);
                    hashMap.put("tel",txt_tel);
                    hashMap.put("imgUrl","default");
                    databaseReference.setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()){
                                Intent intent = new Intent(Register.this, Welcome.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }else{

                                Toast.makeText(Register.this, Objects.requireNonNull(task.getException()).getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                    });


//                    rUser.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
//                        @Override
//                        public void onComplete(@NonNull Task<Void> task) {
//                            if (task.isSuccessful()){
//                                Intent intent = new Intent(Register.this,Menu.class);
//                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NEW_TASK);
//                                startActivity(intent);
//                            }else{
//
//                                Toast.makeText(Register.this, Objects.requireNonNull(task.getException()).getMessage(), Toast.LENGTH_SHORT).show();
//                            }
//                        }
//                    });


                }else{

                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(Register.this, Objects.requireNonNull(task.getException()).getMessage(), Toast.LENGTH_SHORT).show();

                }
            }
        });
    }


}