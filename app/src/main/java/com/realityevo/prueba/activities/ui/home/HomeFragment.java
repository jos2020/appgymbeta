package com.realityevo.prueba.activities.ui.home;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.material.snackbar.Snackbar;
import com.realityevo.prueba.R;
import com.realityevo.prueba.activities.Login;

public class HomeFragment extends Fragment {


    private HomeViewModel homeViewModel;
    CallbackManager callbackManager;
    ShareDialog shareDialog;
    private Button sharePhoto;



    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {


        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        final TextView textView = root.findViewById(R.id.text_home);
        sharePhoto = root.findViewById(R.id.buttonShr);

        callbackManager =  CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);


        homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });

        sharePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareLinkContent content = new ShareLinkContent.Builder()
                        .setQuote("Nice")
                        .setContentUrl(Uri.parse("https://developers.facebook.com"))
                        .build();
                if(ShareDialog.canShow(ShareLinkContent.class)){
                    shareDialog.show(content);
                }
//                Snackbar.make(v, "Hola", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
            }
        });
        return root;
    }
}