package com.realityevo.prueba.activities;

public class Data {
    private  String userId;
    private  String userName;
    private  String lastName;
    private  String emailAddress;
    private  String password;
    private  String tel;
    private  String imgUrl;

    public Data(String userId, String userName, String lastName, String emailAddress, String password, String tel, String imgUrl) {
        this.userId = userId;
        this.userName = userName;
        this.lastName = lastName;
        this.emailAddress = emailAddress;
        this.password = password;
        this.tel = tel;
        this.imgUrl = imgUrl;
    }

    public Data() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
